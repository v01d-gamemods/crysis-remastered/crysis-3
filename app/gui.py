import dearpygui.dearpygui as dpg

from trainerbase.gui import add_codeinjection_to_gui, simple_trainerbase_menu
from injections import *


@simple_trainerbase_menu("Crysis 3 Remastered", 300, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Code Injections", tag="code_injections"):
            add_codeinjection_to_gui(freeze_ammo, "Feeze Ammo")
            add_codeinjection_to_gui(freeze_energy, "Feeze Energy")
            add_codeinjection_to_gui(freeze_lp, "Freeze LP")
